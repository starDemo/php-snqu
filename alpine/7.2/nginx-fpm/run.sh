#!/bin/sh
if [[ "$1" == apache2* ]] || [ "$1" == php-fpm ] || [ "$1" == supervisord ] ; then
    if [ "$(id -u)" = '0' ]; then
        case "$1" in
            apache2*)
                user="${APACHE_RUN_USER:-www-data}"
                group="${APACHE_RUN_GROUP:-www-data}"
                ;;
            *) # php-fpm
                user='www-data'
                group='www-data'
                ;;
        esac
    else
        user="$(id -u)"
        group="$(id -g)"
    fi
    chown www-data:www-data /sessions /var/nginx/client_body_temp
fi
exec "$@"
